import gdal
import gdalconst
import numpy as np
import random
import ogr
from sklearn.model_selection import train_test_split


def last_8chars(x):
    """Extract the last 8 characters of a string"""
    return x[-8:]


def read_file(file_name):
    """Read raster files"""
    return gdal.Open(file_name, gdalconst.GA_ReadOnly)


def geo_transform(raster_file):
    """Gives the geo transformation of a raster file"""
    return raster_file.GetGeoTransform()


def produce_box(raster_file):
    """Gives two corner coordinates (upper left and lower right) of a raster file"""
    gt = raster_file.GetGeoTransform()
    return [gt[0], gt[3], gt[0] + (gt[1] * raster_file.RasterXSize), gt[3] + (gt[5] * raster_file.RasterYSize)]


def intersection(box1, box2):
    """Finds intersection of sets of coordinates"""
    return [min(box1[0], box2[0]), max(box1[1], box2[1]), max(box1[2], box2[2]), min(box1[3], box2[3])]


def calculate_offsets(inv_gts, first_point, second_point):
    """calculate offsets"""
    return list(map(lambda inv_gt: gdal.ApplyGeoTransform(inv_gt, first_point, second_point), inv_gts))


def convert_integer(coordinates):
    """Convert to integer a set/list of coordinates"""
    return list(map(lambda coordinate: list(map(int, coordinate)), coordinates))


def read_first_band(raster_file):
    return raster_file.GetRasterBand(1)


def random_point(x_ref, y_ref, x_min, y_min, x_max, y_max, pixel_size):
    """Function that sreates a random point, that matches with the centroid of a image pixel
    Input: Coordinates of reference of a pixel centroid in the image
           Extent of image
           pixel size
    Output: Coordinates of the point"""
    # Calculation of the minimum point in x and y using  the minimum extent of a protected area. If clause necessary to
    # obtain values in the centroid of landsat pixels. Using point of reference
    # np.random.seed(42)
    if x_min < x_ref:
        x_origen = min(np.arange(x_ref, x_min, -pixel_size))
    elif x_min > x_ref:
        x_origen = max(np.arange(x_ref, x_min, pixel_size))
    else:
        x_origen = x_ref

    if y_min < y_ref:
        y_origen = min(np.arange(y_ref, y_min, -pixel_size))
    elif y_min > y_ref:
        y_origen = max(np.arange(y_ref, y_min, pixel_size))
    else:
        y_origen = y_ref

    # Calculating the upper and higher x and y
    if x_max > x_ref:
        x_top = max(np.arange(x_ref, x_max, pixel_size))
    elif x_max < x_ref:
        x_top = min(np.arange(x_ref, x_max, -pixel_size))
    else:
        x_top = x_ref

    if y_max > y_ref:
        y_top = max(np.arange(y_ref, y_max, pixel_size))
    elif y_max < y_ref:
        y_top = min(np.arange(y_ref, y_max, -pixel_size))
    else:
        y_top = y_ref

    # List of possible values of centroids in x and y
    x_r = np.arange(x_origen, x_top, pixel_size)
    y_r = np.arange(y_origen, y_top, pixel_size)

    # Random selection of a point
    x = random.choice(x_r)
    y = random.choice(y_r)
    return x, y


def create_9grid(x_min, y_min, x_max, y_max, cols, rows, size):
    """Function that creates a grid of rectangles/squares.
     Input: two extreme coordinates: bottom left and top right
            number cols and rows (size of the grid)
            Resolution = size or length of each square (e.g 30m for landsat)
    Output: List of polygons"""
    x_min = float(x_min)
    x_max = float(x_max)
    y_min = float(y_min)
    y_max = float(y_max)
    cols = int(cols)
    rows = int(rows)
    # start grid cell envelope
    ringXleftOrigin = x_min
    ringXrightOrigin = x_min + size
    ringYtopOrigin = y_max
    ringYbottomOrigin = y_max - size
    # create grid cells
    countcols = 0

    poly_list = []
    while countcols < cols:
        countcols += 1

        # reset envelope for rows
        ringYtop = ringYtopOrigin
        ringYbottom = ringYbottomOrigin
        countrows = 0

        while countrows < rows:
            countrows += 1
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(ringXleftOrigin, ringYtop)
            ring.AddPoint(ringXrightOrigin, ringYtop)
            ring.AddPoint(ringXrightOrigin, ringYbottom)
            ring.AddPoint(ringXleftOrigin, ringYbottom)
            ring.AddPoint(ringXleftOrigin, ringYtop)
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)
            poly_list.append(poly)

            # new envelope for next poly
            ringYtop = ringYtop - size
            ringYbottom = ringYbottom - size

            # new envelope for next poly
        ringXleftOrigin = ringXleftOrigin + size
        ringXrightOrigin = ringXrightOrigin + size

    return poly_list


def split_data(x, y):
    """Function that splits data set 50/50
    Input: x= list/array, y = list/array
    Output: Four arrays"""
    Xtrain, Xtest, ytrain, ytest = train_test_split(x, y, random_state=42, test_size=0.5)
    return Xtrain, Xtest, ytrain, ytest


def reshape_ndarray(array):
    """"Function that reshapes each array inside the array([No. Features, cols, rows]) in a list (unify rows and cols)
    and create an array of total pixels (rows*cols) X features
    Input: List of Arrays
    Output: List of Arrays"""
    list = []
    for i in array:
        n = i.flatten()
        list.append(n)
    map_array = np.asarray(list)
    return map_array.transpose()
