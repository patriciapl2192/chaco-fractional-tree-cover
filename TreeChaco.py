# ############################################################################################################# #
# (c) Matthias Baumann, Humboldt-Universität zu Berlin, 4/15/2019                                               #
#  MAP- Geoprocessing with python                                                                  #
#  Ángela Patricia Pérez Lora                                                                                        #
# ####################################### LOAD REQUIRED LIBRARIES ############################################# #

import os
import osr
import glob
import time
import gdal
import gdalconst
import functools
import pandas as pd
import numpy as np
from joblib import Parallel, delayed
from Functions import read_file, produce_box, intersection, random_point, split_data
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error, r2_score
import ogr

# ####################################### SET TIME-COUNT ###################################################### #
starttime = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
print("--------------------------------------------------------")
print("Starting process, time: " + starttime)
print("")


# ####################################### FUNCTIONS ########################################################### #
def reproject_point(x, y, ref_file_in, ref_file_out):
    """Function that changes the coordinate reference system (crs) of a point (x, y).
        Input: point coordinate x and y
               Reference file with the crs to be changed
               Reference file with the crs to be set
        Output: Coordinates of the point with the new crs"""
    # Coordinate System of the input file
    in_pj = osr.SpatialReference()
    in_pj.ImportFromWkt(ref_file_in.GetProjectionRef())

    # Coordinate System of the output file
    out_pj = osr.SpatialReference()
    out_pj.ImportFromWkt(ref_file_out.GetProjectionRef())

    # Transformation factor
    tr = osr.CoordinateTransformation(in_pj, out_pj)

    # Transformation of the point
    X, Y, Z = tr.TransformPoint(x, y)
    return X, Y


def reproject_raster(file, pixel_size, ref_file_out, data_path, file_name):
    """Function that changes the coordinate reference system (crs), and the pixel resolution from an image. The file is
    saved in a given data path.
     Input: File of the image
            pixel size
            File for crs reference
            Data path or directory for storing the .tif generated
            file name
     Output: File and size"""
    # Geo transform and projection from the imput file
    in_pj = file.GetProjectionRef()
    in_gt = file.GetGeoTransform()

    # Projection of the reference file
    out_pj = ref_file_out.GetProjectionRef()

    # Coordinates for upper left and lower right of the imput image
    u_lx, u_ly, l_rx, l_ry = produce_box(file)

    # Transformation of coordinates reference system of the u_lx, u_ly, l_rx, l_ry points
    U_lx, U_ly = reproject_point(u_lx, u_ly, file, ref_file_out)
    L_rx, L_ry = reproject_point(l_rx, l_ry, file, ref_file_out)

    # Driver for the output
    # mem_drv = gdal.GetDriverByName('MEM')

    output_file = data_path + '/' + file_name
    driver = gdal.GetDriverByName('GTiff')

    # New raster X size and Y size
    x_res = int(abs(L_rx - U_lx) / pixel_size)
    y_res = int(abs(U_ly - L_ry) / pixel_size)

    # Checking data typeof the input file,
    band = landsat.GetRasterBand(1)
    # data_type = gdal.GetDataTypeByName(band.DataType)

    # Create new destination data source
    ds = driver.Create(output_file, x_res, y_res, 1, band.DataType)

    # New geo transformation
    ds.SetGeoTransform((U_lx, pixel_size, in_gt[2], U_ly, in_gt[4], -pixel_size))
    ds.SetProjection(out_pj)
    # Re-projection
    gdal.ReprojectImage(file, ds, in_pj, out_pj, gdalconst.GRA_Med)
    return ds, x_res, y_res


def extent_images(files_list):
    """Function that returns upper left (maximum/origin) coordinate and lower right (minimum) coordinate of several
    images = Extent
    Input: List of files
    Output: Extent of all images together """
    boxes = list(map(produce_box, files_list))
    return functools.reduce(intersection, boxes)


def check_point_int_box(raster_file, x, y):
    """Function that checks if a point is inside the extent of a raster file (box: using the upper left and the lower
    right coordinates)
    Input: Raster file
           Coordinates of the point
    Output: Boolean. True/False"""
    box = produce_box(raster_file)
    return (x < box[2]) & (x > box[0]) & (y < box[1]) & (y > box[3])


def identify_file(raster_files, x, y):
    """Function that search and returns the file that contains the point (x, y)
       Input: List of raster files
              Coordinates of the point
       Output: Raster file"""
    for file in raster_files:
        if check_point_int_box(file, x, y):
            return file
        else:
            pass


def classify_array(array, no_classes):
    """Function that classifies an array into a given number of classes. the clasasification made is distributed
    uniformly using the maximum and minimum value of the array
    Input: array
           number of classes: int
    Output: array with the classification"""
    class_arr = array.copy()
    min = array.min()
    max = array.max()
    rank = (max - min) / no_classes
    lim_inf = min
    lim_sup = min + rank
    for i in range(no_classes):
        class_arr[(class_arr >= lim_inf) & (class_arr < lim_sup)] = i
        lim_inf = lim_sup
        lim_sup = lim_inf + rank
    return class_arr


def translate_point(raster_file, x, y):
    """Function that obtains the array index for an specific set of coordinates in a given raster file
    Input: Raster file
           Coordinates of the point
    Output: Index (x, y)"""
    gt = raster_file.GetGeoTransform()
    px = int(abs(x - gt[0]) / abs(gt[1]))  # x pixel
    py = int(abs(y - gt[3]) / abs(gt[5]))  # y pixel
    return px, py


def check_na_arr(raster_file, x, y):
    """Helper function that returns true if the value of a raster pixel is -9999 in all layers at the point (x, y)
    Input: raster file
           Coordinates of the point
    Output: Boolean True/False"""
    X, Y = translate_point(raster_file, x, y)
    value = raster_file.ReadAsArray(X, Y, 1, 1).flatten()
    return all(value == -9999)


def no_data(array):
    """Helper function to extract the indices where the value is -9999
    Input: array of 1 dimension (List of values)
    Output: indices where the value = -9999 and indices where the data is available"""
    return np.concatenate(np.where(array == -9999)), np.concatenate(np.where(array != -9999))


def interpolate(array):
    """Function that interpolates the empty values of a given array (list). Method: Linear interpolation
    Input: array of 1 dimension (List)
    Output: Interpolated array"""
    x, xp = no_data(array)
    fp = array[xp]
    values = np.interp(x, xp, fp)
    np.set_printoptions(precision=3)
    array[x] = values
    return array


def filling_gaps_image_layers(image_arrays):
    """Function that interpolates (per row) the empty values of a list of arrays if there are at least 50% of values available
    per row. Method: Linear interpolation
    Input: List of arrays
    Output: List of arrays"""
    all_matrices = []
    for matrix in image_arrays:
        flatted_matrix = list(matrix.flatten())
        all_matrices.append(flatted_matrix)
    all_matrices = np.asarray(all_matrices).transpose()
    for row in all_matrices:
        condition = (row != -9999).sum()
        if condition >= 0.5 * len(row):
            interpolate(row)
        else:
            pass
    return all_matrices


def reshape_arr(array, y_size):
    """ Function that changes the dimensions of an array using the n = number of values in the array and a desired
    number of columns
    Input: array
           y_size = number of columns
    Output: array"""
    n = array.shape
    x_size = int(n[0]) / y_size
    return array.reshape(int(x_size), int(y_size))


def save_array_image(array, data_path, file_origin):
    """Save array as an image using an image the origin image as reference for crs and gt
    Input: array
           name of file
           number for the name of the file
           file of origin = reference image file
    Output: Image saved in """
    shape = array.shape
    n_rows = shape[0]
    n_cols = shape[1]
    drv = gdal.GetDriverByName('GTiff')
    dst_ds = drv.Create(data_path + '_RF-results.tif', n_rows, n_cols, 1, gdalconst.GDT_Float64, )
    dst_ds.SetGeoTransform(file_origin.GetGeoTransform())
    dst_ds.SetProjection(file_origin.GetProjectionRef())
    dst_ds.GetRasterBand(1).WriteArray(array)
    dst_ds.FlushCache()

# ####################################### FOLDER PATHS & global variables ##################################### #

# Direction where the folder containing all files to work with
data_path = '/home/patricia/INRM/Fourth Semester/GeoPython/Exam/MAP_data'

# MODIS files
modis_names = glob.glob(os.path.join(data_path + "/MODIS_EVI", '*.tif'))
modis_files = list(map(read_file, modis_names))
modis_arr = list(map(lambda x: x.ReadAsArray(), modis_files))

# Landsat file
landsat_name = glob.glob(os.path.join(data_path + "/CHACO_TreeCover2015", '*.tif'))
landsat = read_file(landsat_name[0])

# Other Landsat file
landsat_nameII = glob.glob(os.path.join(data_path + "/Landsat_TC", '*.bsq'))
landsatII = read_file(landsat_nameII[0])

# MODIS file data (X_size and Y_size)
modis_sample = modis_files[1]
gt = modis_sample.GetGeoTransform()
x_size = gt[1]
y_size = gt[5]

# Projection
pj = modis_sample.GetProjectionRef()

# ####################################### PROCESSING ########################################################## #
# Goal: Fractional tree cover Map of el Chaco using the Landsat-based tree cover map (Baumann et al., 2018)
# Remote Sensing of Environment
# The processing is divided in the next sections:
# 1. Re-sampling the Landsat image: adjust resolution to the MODIS images
# 2. Collection of training data: 500 points across the re-sampled Landsat image (50 points per class)
# 3. Mining of MODIS images: Filling gaps with interpolation and replacing unknown values with 0
# 4. Regression
# 5. Saving results
# ####

# #### 1. Re-sampling of Landsat image: Fractional tree cover

# Re-sampling using gdal.Warp, time: 2min, 1 sec
# Re-sample Landsat resolution and crs: use x and y size from the MODIS sample data. re-sample algorithm: med(takes the
# median value and change the reference system to the projection of the MODIS sample
# https://gdal.org/programs/gdalwarp.html

# landsat_resample = gdal.Warp('CHACO_TreeCover2015_Resample.tif', landsat, xRes=x_size, yRes=y_size, resampleAlg='med',
#                             dstSRS=pj)

# Re-sampling with my method, time: 2min, 4 sec
# Re-sample Landsat resolution and crs using resolution of MODIS sample.

#landsat_resample = reproject_raster(landsat, abs(x_size), modis_sample, data_path, 'Chaco_TreeCover_ResampleII.tif')

landsat_resample = read_file(data_path + '/Chaco_TreeCover_ResampleII.tif')

# #### 2. Collection of training data

# The training points will be generated randomly in the centroid of a MODIS pixel in the extent formed by all the MODIS
# files. 50 per strata

# Classification
landsat_arr = landsat_resample.ReadAsArray()
no_classes = 10
classi = classify_array(landsat_arr, no_classes)

landsat_arr = None
# Extent of the composite of all MODIS images
extent = extent_images(modis_files)

# Centroid for the upper left extreme pixel. Used as reference for creating the random points
x_cent = extent[0] + (gt[1] / 2)
y_cent = extent[1] + (gt[1] / 2)

# Point shape file
driver = ogr.GetDriverByName('ESRI shapefile')
ds = driver.CreateDataSource(data_path + "/Output")
crs = osr.SpatialReference()
crs.ImportFromWkt(pj)
out_lyr = ds.CreateLayer('pointsChacolero', crs, ogr.wkbPoint)
featureDefn = out_lyr.GetLayerDefn()

# Initializing training points count and lists with the training data
tree_train = []
evi_train = []
class_p = []

for i in range(no_classes):
    point_counter = 0

    while point_counter <= 49:
        # Creating random point inside the MODIS images extent
        point = random_point(x_cent, y_cent, extent[0], extent[3], extent[2], extent[1], gt[1])
        x = point[0]
        y = point[1]

        # Obtaining index of the point in landsat image
        x_land, y_land = translate_point(landsat_resample, x, y)

        # Checking if the random point belongs to the class
        class_point = classi[y_land, x_land]
        if class_point == i:
            # Identification of the specific image that contains the random point generated
            image = identify_file(modis_files, x, y)
            # Checking that the point generated has no empty values in the MODIS image
            if check_na_arr(image, x, y) is False:

                # Creating point
                point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint(x, y)

                # Creating point in layer
                out_feat = ogr.Feature(featureDefn)
                out_feat.SetGeometry(point)
                out_lyr.CreateFeature(out_feat)

                # Update counter of training points
                point_counter += 1
                class_p.append(class_point)

                # Values of fractional tree in landsat image at the training point
                frac_tree_train = landsat_resample.ReadAsArray(x_land, y_land, 1, 1).flatten()
                tree_train.append(frac_tree_train)

                # Obtaining index of the point in the MODIS image
                x_modis, y_modis = translate_point(image, x, y)

                # Values of EVI in MODIS image at the training point
                evi_mod_train = image.ReadAsArray(x_modis, y_modis, 1, 1).flatten()
                evi_train.append(evi_mod_train)

            else:
                pass
        else:
            pass

print("Shape file ready")
# flush data to disk, set the NoData
ds.FlushCache()
ds = None
classi = None

tree_train = pd.DataFrame(tree_train)
evi_train = pd.DataFrame(evi_train)

# Interpolation of empty training points
evi_train_n = np.apply_along_axis(interpolate, axis=1, arr=evi_train)

# #### 3. Mining of Modis images
# Interpolation of gaps
modis_arr_inter = list(map(filling_gaps_image_layers, modis_arr))

modis_arr = None

# Replacing empty values with 0
imp_null = SimpleImputer(missing_values=-9999, strategy='constant', fill_value=0)
[imp_null.fit(array) for array in modis_arr_inter]
modis_arr_inter_new = [imp_null.transform(arr) for arr in modis_arr_inter]
modis_arr_inter = None

# #### 4. Regression
# Training data
X = evi_train_n
y = tree_train[0]
X_scaled = StandardScaler().fit(X).transform(X)

# Splitting data
Xtrain, Xtest, ytrain, ytest = split_data(X_scaled, y)

print("Finding Model Parameters")

# Grid search of best parameters. The parameters chosen to be varied are: Number of trees, tree depth, and learning rate
param_grid = {'n_estimators': [5, 10, 50, 100, 500, 1000], 'learning_rate': [0.1, 0.15, 0.05, 0.2, 0.25, 0.001],
              'max_depth': [1, 2, 3, 4], 'min_samples_leaf': [1, 5, 10, 50]}
grid = GridSearchCV(estimator=GradientBoostingRegressor(random_state=0, loss='ls'), param_grid=param_grid)
grid.fit(Xtrain, ytrain)
print(grid.best_params_)

# Fit model
model = grid.best_estimator_
yfit = model.predict(Xtest)
model_score = model.score(Xtrain, ytrain)
print('R2 sq: ', model_score)
print("Mean squared error: %.2f" % mean_squared_error(y_true=ytest, y_pred=yfit))
print('Test Variance score: %.2f' % r2_score(ytest, yfit))

Xtrain, Xtest, ytrain, ytest, X, y, X_scaled, yfit = None, None, None, None, None, None, None, None

# Scaling input data
map_scaled_all = list(map(lambda arr: StandardScaler().fit(arr).transform(arr), np.array(modis_arr_inter_new)))

# Prediction

# Prediction without parallelizing
#y_map = list(map(model.predict, map_scaled_all))
y_map = Parallel(n_jobs=3)(delayed(model.predict)(i) for i in map_scaled_all)
modis_arr_inter_new = None

print("Done Regression")

# #### 5. Saving Images

# Reshaping arrays
images = list(map(lambda arr: reshape_arr(arr, 1000), y_map))

# Saving
for i in range(len(modis_files)):
    save_array_image(images[i], data_path + '/Output/'+str(i), modis_files[i])

print("Finished")

# ####################################### END TIME-COUNT AND PRINT TIME STATS################################## #
print("")
endtime = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
print("--------------------------------------------------------")
print("start: " + starttime)
print("end: " + endtime)
print("")
